### Projet Memory Game

## Description
Le projet Memory Game est une application de jeu de mémoire classique développée avec HTML, CSS et TypeScript. Ce jeu vise à stimuler la mémoire et la concentration des joueurs en les mettant au défi de trouver des paires correspondantes parmi un ensemble de cartes.

## Fonctionnalités principales
- **Interface intuitive :** Une interface conviviale permet aux joueurs de naviguer facilement dans le jeu.
- **Différents niveaux de difficulté :** Choisissez entre différents niveaux pour adapter la complexité du jeu.
- **Animations fluides :** Des animations agréables rendent l'expérience de jeu plus immersive.

## Technologies utilisées
- HTML
- CSS
- TypeScript

## Installation
1. Clonez le dépôt `git clone https://github.com/votre_utilisateur/memory-game.git`
2. npm install.
2. Ouvrez le fichier `index.html` dans votre navigateur web.


## Auteurs
Chems MEZIOU
