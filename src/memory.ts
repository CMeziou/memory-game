const startGame = document.querySelector<HTMLButtonElement>(".control-buttons span");
let nameOfPlayer = document.querySelector<HTMLDivElement>(".name span");
const removeSplashScreen = document.querySelector<HTMLButtonElement>(".control-buttons");
const blocksContainer: any = document.querySelector<HTMLDivElement>(".memory-game-blocks");
let blocks = Array.from(document.querySelector<HTMLDivElement>(".game-block").children);
let triesElement: any = document.querySelector('.tries span');
const duration: number = 1000;

//le Array.from() transform blocks en tableau qui contient blocksContainer.children(les enfants de blocksContainer)

//pour faire avec des objets, il faut générer le html directement à partir du typescript et devoir le manupuler directment d'ici



let orderRange: number[] = Array.from(Array(blocks.length).keys());

shuffle(orderRange);

/**
 * enleve la div avec le bouton Start
 * @param {}
 * 
 */
if (startGame) {
    startGame.addEventListener('click', () => {
        let yourName: string | null = prompt("What's your name");
        if (yourName == null || yourName == "") {
            if(nameOfPlayer)
            nameOfPlayer.innerHTML = 'unknown';
        } else {
            nameOfPlayer.innerHTML = yourName;
        }
        removeSplashScreen?.remove();
    })
}

// for (const block of blocks) {
//     block.style.order = orderRange.indexOf(block);
//     console.log(block);

// }
blocks.forEach((block: any, index: number | string) => {
    //ajoute la proprieter CSS order
    block.style.order = orderRange[index];

    //ajoute un click event
    block.addEventListener('click', () => {
        //trigger(déclecnher) the flip block
        flipBlock(block);
    })
});

//flip(retourner) Block function``
/**
 * 
 * @param selectedBlock 
 */
function flipBlock(selectedBlock: { classList: { add: (arg0: string) => void; }; }) {
    //add css class is-flipped
    selectedBlock.classList.add('is-flipped');

    //collect all flipped cards
    let allFlippedBlocks = blocks.filter((flippedBlock: { classList: { contains: (arg0: string) => any; }; }) => flippedBlock.classList.contains('is-flipped'));

    if (allFlippedBlocks.length === 2) {
        //stop clicking function
        stopClicking();

        //check matched block
        checkMatchedBlock(allFlippedBlocks[0], allFlippedBlocks[1]);
    }

}

// function qui va mélanger les photo en random
/**
 * 
 * @param array 
 * @returns 
 */
function shuffle(array: any[]) {
    let current: number = array.length;
    let temp: number;
    let random: number;

    while (current > 0) {

        //obtenir un nombre aléatiore
        random = Math.floor(Math.random() * current);

        //diminuer la longeur du tableau contenu dans la variable current
        current--;

        //sauvegarder le current element dans la variable temp
        temp = array[current];

        //le current element = l'element random
        array[current] = array[random];

        //l'element current = la valeur sauvegarder dans la variable temporaire
        array[random] = temp;
    }
    return array;
}

//stop clicking function

/**
 * 
 */
function stopClicking() {
    //add class no clicking on main container
    blocksContainer.classList.add('no-clicking')
    //vouloir la faire en TS

    setTimeout(() => {
        //remove class no-cllicking after 1 sec
        blocksContainer.classList.remove('no-clicking')
    }, duration);
}

//check matched block

/**
 * 
 * @param firstBlock 
 * @param secondBlock 
 */
function checkMatchedBlock(firstBlock: any, secondBlock: any) {
    if (firstBlock.dataset.technology === secondBlock.dataset.technology) {
        firstBlock.classList.remove('is-flipped');
        secondBlock.classList.remove('is-flipped');

        firstBlock.classList.add('has-match');
        secondBlock.classList.add('has-match');
    } else {
        if (triesElement) {
            triesElement.innerHTML = parseInt(triesElement.innerHTML) + 1;
        }
        setTimeout(() => {
            firstBlock.classList.remove('is-flipped');
            secondBlock.classList.remove('is-flipped');
        }, duration)
    }
}